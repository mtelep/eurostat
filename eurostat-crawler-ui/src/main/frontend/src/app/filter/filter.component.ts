import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  submitted: boolean = false;

  model: Map<string, any>;
  error: any;

  products: string = '84869090';
  threshold: number = 900000;

  public onSubmit() : void {
    this.model = null;
    this.error = null;
    this.kickIt();
  }

  private kickIt() : void {
    this.submitted = true;
    const prodList: string = this.products.trim().split(/\s/g).join(',');
    console.log('Product list:', prodList);

    this.http.get('http://localhost:8080/eurostat?product=' + prodList)
    // this.http.get('http://localhost:8080/eurostat/crawl')
      .subscribe(
        (r: Response) => {
          this.submitted = false;
          this.model = r.json().products;
          console.log('Response', JSON.stringify(this.model));
        },
        (error) => {
          this.error = error.json();
          console.log("Error", this.error);
          this.submitted = false;
        });
  }

  constructor(private http: Http) { }

  ngOnInit() {
  }

  get debug() {
    return JSON.stringify(this.model);
  }

  get entries(): Array<string> {
    if(this.model) {
      return Object.keys(this.model);
    }
    return [""];
  }

  public getProduct(id: number) : any {
    return this.model[id];
  }

}
