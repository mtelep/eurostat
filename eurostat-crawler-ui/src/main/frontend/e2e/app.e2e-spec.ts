import { EurostatCrawlerUiPage } from './app.po';

describe('eurostat-crawler-ui App', function() {
  let page: EurostatCrawlerUiPage;

  beforeEach(() => {
    page = new EurostatCrawlerUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
