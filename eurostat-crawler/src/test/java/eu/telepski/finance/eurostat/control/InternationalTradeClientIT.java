package eu.telepski.finance.eurostat.control;

import eu.telepski.finance.eurostat.message.control.EurostatMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.io.*;
import java.util.zip.ZipInputStream;

import static org.junit.Assert.assertNotNull;

/**
 * @author memnit0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class InternationalTradeClientIT {

    @Inject
    private InternationalTradeClient client;

    @Test
    public void shouldUnmarshal() throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        new BufferedReader(new FileReader("src/test/resources/response.xml")).lines().forEach(sb::append);

        String xml = sb.toString();

        EurostatMessage msg = client.unmarshal(xml);

        assertNotNull(msg);
        assertNotNull("payload should not be null", msg.getPayload());
    }

    @Test
    public void shouldUnmarshalZip() throws IOException {
        ZipInputStream str = new ZipInputStream(new FileInputStream("src/test/resources/response.zip"));

        assertNotNull(client.fromZip(str));
    }
}
