package eu.telepski.finance.eurostat.control;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Frog on 2017-04-16.
 */
public class NanToNullConverterTest {

    private NanToNullConverter converter;

    @Before
    public void setUp() {
        converter = new NanToNullConverter();
    }

    @Test
    public void shouldConvertNanToNull() throws Exception {
        String result = converter.unmarshal("NaN");
        assertNull(result);
    }

    @Test
    public void shouldConvertNullToNan() throws Exception {
        String result = converter.marshal(null);
        assertEquals("NaN", result);
    }

    @Test
    public void unmarshalShouldNotModifyValue() throws Exception {
        String input = "klops";
        String result = converter.unmarshal(input);
        assertEquals(input, result);
    }

    @Test
    public void marshalShouldNotModifyValue() throws Exception {
        String input = "aaa";
        String result = converter.marshal(input);
        assertEquals(input, result);
    }
}