package eu.telepski.finance.csv.control;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.springframework.stereotype.Component;

/**
 * @author memnit0
 */
@Slf4j
@Component
public class CsvWriter {

    CsvWriter() {}

    public void write(Object object) {
        String csv = CSVFormat.EXCEL.format(object);
        log.info("CSV\n{}", csv);
    }
}
