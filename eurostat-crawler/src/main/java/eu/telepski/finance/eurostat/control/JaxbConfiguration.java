package eu.telepski.finance.eurostat.control;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import eu.telepski.finance.eurostat.message.control.EurostatMessage;

/**
 * @author memnit0
 */
@Configuration
public class JaxbConfiguration {

    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan(EurostatMessage.class.getPackage().getName());
        return marshaller;
    }
}
