package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import eu.telepski.finance.eurostat.control.NanToNullConverter;
import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "ObsValue", namespace = EurostatMessage.GENERIC_NAMESPACE)
@XmlAccessorType(FIELD)
public class DataValue {

    @XmlAttribute(name = "value")
    @XmlJavaTypeAdapter(NanToNullConverter.class)
    private String value;
}
