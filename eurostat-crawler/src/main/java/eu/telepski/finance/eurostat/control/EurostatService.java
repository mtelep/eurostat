package eu.telepski.finance.eurostat.control;

import com.google.common.collect.Lists;
import eu.telepski.finance.eurostat.message.control.DataSeries;
import eu.telepski.finance.eurostat.message.control.EurostatMessage;
import eu.telepski.finance.eurostat.message.control.GenericValue;
import eu.telepski.finance.eurostat.message.control.SeriesKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyMap;
import static org.springframework.http.ResponseEntity.ok;

/**
 * @author memnit0
 */
@Slf4j
@Service
public class EurostatService {

    private final InternationalTradeClient client;
    private final TaskExecutor executor;

    @Inject
    EurostatService(InternationalTradeClient client, TaskExecutor executor) {
        this.client = client;
        this.executor = executor;
    }

    public ResponseEntity<EurostatResponse> request(InternationalTradeQueryFilter filter) {

        Map<String, List<ProductData>> products = Collections.synchronizedMap(new HashMap<>());

        int subListSize = 1;
        List<List<String>> subLists = Lists.partition(filter.getProducts(), subListSize);
        AtomicInteger counter = new AtomicInteger(0);

        subLists.stream()
                .map(p -> this.subFilter(filter, p))
                .map(client::request)
                .map(this::handleEurostatMessage)
                .map(ResponseEntity::getBody)
                .forEach(er -> {
                    products.putAll(Optional.ofNullable(er.getProducts()).orElse(emptyMap()));
                    log.info("Processed {}/{} requests", counter.incrementAndGet(), subLists.size());
                    sleep(TimeUnit.SECONDS.toMillis(15L));
                });

        EurostatResponse response = EurostatResponse.builder()
                .eurostatResponseCode(HttpStatus.OK.value())
                .products(products)
                .additionalEurostatMessage("haha!")
                .build();

        return ResponseEntity.ok(response);
    }

    private InternationalTradeQueryFilter subFilter(InternationalTradeQueryFilter filter, List<String> products) {
        return InternationalTradeQueryFilter.builder()
                .flows(filter.getFlows())
                .indicators(filter.getIndicators())
                .partners(filter.getPartners())
                .period(filter.getPeriod())
                .reporters(filter.getReporters())
                .products(products)
                .build();
    }

    private ResponseEntity<EurostatResponse> handleEurostatMessage(EurostatMessage msg) {
        if(msg.getPayload() == null || msg.getPayload().getSeries() == null) {
            ResponseEntity<EurostatResponse> response = ResponseEntity.status(msg.getFooter().getMessageCode().getCode())
                    .body(EurostatResponse.builder()
                            .eurostatResponseCode(msg.getFooter().getMessageCode().getCode())
                            .additionalEurostatMessage(String.join("\n", msg.getFooter().getMessageCode().getMessageText()))
                            .build());

            if(isAccepted(msg)) {
                msg = client.poll(readPollUrl(msg));
            } else {
                return response;
            }
        }

        Map<String, List<ProductData>> products = buildProducts(msg);

        EurostatResponse response = EurostatResponse.builder()
                .products(products)
                .build();

        response.getProducts().entrySet()
                .forEach(e -> e.getValue().sort((l, r) -> r.getSum().compareTo(l.getSum())));

        return ok(response);
    }

    private Map<String, List<ProductData>> buildProducts(EurostatMessage msg) {
        Map<String, List<ProductData>> products = new LinkedHashMap<>();
        msg.getPayload().getSeries().forEach(ds -> {
            ProductData productData = ProductData.builder()
                    .partner(readPartner(ds.getKey()))
                    .product(readProduct(ds.getKey()))
                    .dataSeries(prepareData(ds))
                    .build();

            if(!products.containsKey(productData.getProduct())) {
                products.put(productData.getProduct(), new ArrayList<>());
            }

            products.get(productData.getProduct()).add(productData);
        });
        return products;
    }

    private String readPollUrl(EurostatMessage msg) {
        List<String> messages = msg.getFooter().getMessageCode().getMessageText();

        return messages.stream()
                .filter(s -> s.startsWith("http"))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(
                        String.format("Unable to parse poll URL from message %s", messages)));
    }

    private boolean isAccepted(EurostatMessage msg) {
        return msg.getFooter().getMessageCode().getCode() == HttpStatus.PAYLOAD_TOO_LARGE.value();
    }

    private List<Double> prepareData(DataSeries ds) {
        return ds.getData().stream()
                .map(dv -> Optional.ofNullable(dv.getValue().getValue())
                        .map(Double::valueOf)
                        .orElse(0.0D))
                .collect(Collectors.toList());
    }

    private String readPartner(SeriesKey key) {
        return readSeriesKeyValue("PARTNER", key);
    }

    private String readProduct(SeriesKey key) {
        return readSeriesKeyValue("PRODUCT", key);
    }

    private String readSeriesKeyValue(String lookup, SeriesKey key) {
        return key.getValues().stream()
                .filter(value -> value.getId().equals(lookup))
                .findFirst()
                .map(GenericValue::getValue)
                .orElseThrow(() -> new IllegalStateException("Value must be present"));
    }

    public ResponseEntity<EurostatResponse> crawl() {

        List<String> products = readProductCodesFromFile("products.txt");
        final long productsSize = products.size();
        log.info("Products to read: {}", productsSize);
        AtomicLong counter = new AtomicLong(0L);

        Map<String, List<ProductData>> productMap = Collections.synchronizedMap(new HashMap<>());

        Stream<CompletableFuture<Void>> requestsStream = Lists.partition(products, 10).stream()
                .map(productCodes -> CompletableFuture
                        .supplyAsync(() -> {
                            sleep(TimeUnit.SECONDS.toMillis(60));
                            return this.requestProduct(productCodes);
                        }, executor)
                        .thenAccept(er -> {
                            log.info("ER: {}", er);
                            productMap.putAll(er.getProducts());
                            log.info("Processed {}/{} product requests...", counter.addAndGet(er.getProducts().size()), productsSize);
                        })
                        .exceptionally(t -> {
                            log.error("Unable to read product {} from eurostat", productCodes, t);
                            return null;
                        }));

        CompletableFuture<?>[] collect = requestsStream.toArray(CompletableFuture<?>[]::new);

        try {
            CompletableFuture.allOf(collect).get();
            return ok(EurostatResponse.builder()
                    .eurostatResponseCode(200)
                    .additionalEurostatMessage("haha!")
                    .products(productMap)
                    .build());
        } catch (Exception e) {
            throw new EurostatServiceException("Error while receiving results from eurostat", e);
        }
    }

    private void sleep(long millis) {
        try {
            log.debug("Sleeping for {} millis", millis);
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            log.error("Interrupted during sleep", e);
        }
    }

    protected List<String> readProductCodesFromFile(String fileName) {
        try(InputStream stream = getClass().getClassLoader().getResourceAsStream(fileName); BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            return reader.lines().collect(Collectors.toList());
        } catch(IOException e) {
            throw new EurostatServiceException("Unable to read product codes from file", e);
        }
    }

    private EurostatResponse requestProduct(List<String> productCodes) {

        log.info("Requesting products {}", productCodes);
        ResponseEntity<EurostatResponse> response =  request(InternationalTradeQueryFilter.builder()
                .flows(EnumSet.of(InternationalTradeQueryFilter.Flow.EXPORT))
                .indicators(EnumSet.of(InternationalTradeQueryFilter.Indicator.VALUE_IN_EUROS))
                .period(InternationalTradeQueryFilter.Period.MONTHLY)
                .reporters(Collections.singletonList("PL"))
                .products(productCodes)
                .build());

        if(response.getStatusCode() != HttpStatus.OK || response.getBody().getProducts().isEmpty()) {
            log.error("Unable to read product data from eurostat: {}. Response was {}", productCodes, response);
        }

        return response.getBody();
    }
}
