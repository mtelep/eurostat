package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "Header", namespace = EurostatMessage.MSG_NAMESPACE)
@XmlAccessorType(FIELD)
public class MessageHeader {

    @XmlElement(name = "Prepared", namespace = EurostatMessage.MSG_NAMESPACE)
    private String prepared;
}
