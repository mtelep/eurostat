package eu.telepski.finance.eurostat.control;

import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.Map;

/**
 * @author memnit0
 */
@Value
@Builder
public class EurostatResponse {

    private final Integer eurostatResponseCode;
    private final Map<String, List<ProductData>> products;
    private final String additionalEurostatMessage;
}
