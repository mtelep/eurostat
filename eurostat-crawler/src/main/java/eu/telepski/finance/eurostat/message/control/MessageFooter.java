package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "Footer", namespace = EurostatMessage.FOOTER_NAMESPACE)
@XmlAccessorType(FIELD)
public class MessageFooter {

    @XmlElement(name = "Message", namespace = EurostatMessage.FOOTER_NAMESPACE)
    private MessageCode messageCode;
}
