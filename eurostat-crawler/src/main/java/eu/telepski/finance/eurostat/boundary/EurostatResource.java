package eu.telepski.finance.eurostat.boundary;

import eu.telepski.finance.eurostat.control.EurostatResponse;
import eu.telepski.finance.eurostat.control.EurostatService;
import eu.telepski.finance.eurostat.control.InternationalTradeQueryFilter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import static eu.telepski.finance.eurostat.control.InternationalTradeQueryFilter.Flow.EXPORT;
import static eu.telepski.finance.eurostat.control.InternationalTradeQueryFilter.Indicator.VALUE_IN_EUROS;
import static eu.telepski.finance.eurostat.control.InternationalTradeQueryFilter.Period.MONTHLY;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * @author memnit0
 */
@RestController
@RequestMapping("/eurostat")
@CrossOrigin
public class EurostatResource {

    private final EurostatService service;

    @Inject
    EurostatResource(EurostatService service) {
        this.service = service;
    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<EurostatResponse> msg(
            @RequestParam(value = "product", required = true) @Size(min = 1) List<String> products) {

        InternationalTradeQueryFilter filter = InternationalTradeQueryFilter.builder()
                .period(MONTHLY)
                .flows(EnumSet.of(EXPORT))
                .indicators(EnumSet.of(VALUE_IN_EUROS))
                .reporters(Collections.singletonList("PL"))
//                .partners(Lists.newArrayList("NL", "AT"))
                .products(products)
                .build();

        return service.request(filter);
    }

    @GetMapping(value = "/crawl", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<EurostatResponse> crawl() {
        return service.crawl();
    }
}
