package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "SeriesKey", namespace = EurostatMessage.GENERIC_NAMESPACE)
@XmlAccessorType(FIELD)
public class SeriesKey {

    @XmlElement(name = "Value", namespace = EurostatMessage.GENERIC_NAMESPACE)
    private List<GenericValue> values;
}
