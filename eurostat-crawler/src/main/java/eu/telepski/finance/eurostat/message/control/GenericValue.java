package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "Value", namespace = EurostatMessage.GENERIC_NAMESPACE)
@XmlAccessorType(FIELD)
public class GenericValue {

    @XmlAttribute(name = "id")
    private String id;
    @XmlAttribute(name = "value")
    private String value;
}
