package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "Message", namespace = EurostatMessage.FOOTER_NAMESPACE)
@XmlAccessorType(FIELD)
public class MessageCode {

    @XmlAttribute(name = "code")
    private int code;

    @XmlAttribute(name = "severity")
    private String severity;

    @XmlElement(name="Text", namespace = EurostatMessage.COMMON_NAMESPACE)
    private List<String> messageText;
}
