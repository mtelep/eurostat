package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "GenericData", namespace = EurostatMessage.MSG_NAMESPACE)
@XmlAccessorType(FIELD)
public class EurostatMessage {

    static final String MSG_NAMESPACE = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message";
    static final String FOOTER_NAMESPACE = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer";
    static final String GENERIC_NAMESPACE = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic";
    static final String COMMON_NAMESPACE = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common";

    @XmlElement(name = "Header", namespace = MSG_NAMESPACE)
    private MessageHeader header;

    @XmlElement(name = "Footer", namespace = FOOTER_NAMESPACE)
    private MessageFooter footer;

    @XmlElement(name = "DataSet", namespace = MSG_NAMESPACE)
    private MessagePayload payload;
}
