package eu.telepski.finance.eurostat.control;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author memnit0
 */
@Data
@Builder
public class ProductData {

    private final String product;
    private final String partner;
    private final List<Double> dataSeries;

    public Double getSum() {
        return dataSeries.stream().reduce((l, r) -> l + r).orElse(0.0D);
    }
}
