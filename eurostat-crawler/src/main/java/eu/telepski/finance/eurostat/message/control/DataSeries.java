package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "Series", namespace = EurostatMessage.GENERIC_NAMESPACE)
@XmlAccessorType(FIELD)
public class DataSeries {

    @XmlElement(name = "SeriesKey", namespace = EurostatMessage.GENERIC_NAMESPACE)
    private SeriesKey key;

    @XmlElement(name = "Obs", namespace = EurostatMessage.GENERIC_NAMESPACE)
    private List<DataElement> data;
}
