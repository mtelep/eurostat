package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement(name = "ObsDimension", namespace = EurostatMessage.GENERIC_NAMESPACE)
@XmlAccessorType(FIELD)
public class DataDimension {

    @XmlAttribute(name = "value", required = true)
    private String value;
}
