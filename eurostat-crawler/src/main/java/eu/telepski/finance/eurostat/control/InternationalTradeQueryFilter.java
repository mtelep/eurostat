package eu.telepski.finance.eurostat.control;

import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
public class InternationalTradeQueryFilter {
    private final Period period;
    private final List<String> reporters;
    private final List<String> partners;
    private final List<String> products;
    private final Set<Flow> flows;
    private final Set<Indicator> indicators;

    @Getter
    @AllArgsConstructor(access = PRIVATE)
    public enum Period {
        ANNUAL("A"),
        SEMI_ANNUAL("S"),
        HALF_YEAR("H"),
        QUARTERLY("Q"),
        MONTHLY("M"),
        WEEKLY("W"),
        DAILY("D");

        private final String code;
    }

    @Getter
    @AllArgsConstructor(access = PRIVATE)
    public enum Flow {
        IMPORT(1),
        EXPORT(2);

        private final int code;
    }

    @Getter
    @AllArgsConstructor(access = PRIVATE)
    public enum Indicator {
        VALUE_IN_EUROS,
        QUANTITY_IN_100KG,
        SUPPLEMENTARY_QUANTITY
    }
}