package eu.telepski.finance.eurostat.control;

import java.util.Optional;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author memnit0
 */
public class NanToNullConverter extends XmlAdapter<String, Object> {

    @Override
    public String unmarshal(String v) throws Exception {
        if("NaN".equals(v)) {
            return null;
        }
        return v;
    }

    @Override
    public String marshal(Object v) throws Exception {
        return Optional.ofNullable(v)
                .map(Object::toString)
                .orElse("NaN");
    }
}
