package eu.telepski.finance.eurostat.control;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import static java.util.concurrent.Executors.newScheduledThreadPool;

/**
 * @author memnit0
 */
@Configuration
public class SchedulerConfiguration {

    @Bean
    public TaskExecutor taskScheduler() {
        return new ConcurrentTaskScheduler(newScheduledThreadPool(5));
    }
}
