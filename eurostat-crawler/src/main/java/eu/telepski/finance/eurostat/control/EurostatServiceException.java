package eu.telepski.finance.eurostat.control;

/**
 * @author memnit0
 */
public class EurostatServiceException extends RuntimeException {

    public EurostatServiceException(String message) {
        super(message);
    }

    public EurostatServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
