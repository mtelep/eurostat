package eu.telepski.finance.eurostat.control;

import static com.google.common.collect.Lists.newArrayList;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.io.*;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.ZipInputStream;

import javax.inject.Inject;
import javax.xml.transform.stream.StreamSource;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import eu.telepski.finance.eurostat.message.control.EurostatMessage;
import eu.telepski.finance.eurostat.message.control.MessageCode;
import eu.telepski.finance.eurostat.message.control.MessageFooter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author memnit0
 */
@Slf4j
@Component
public class InternationalTradeClient {

    private static final String INTERNATIONAL_TRADE_DB_ID = "DS-016890";
    private static final int MAX_POLL_RETRIES = 500;
    private static final long POLL_INTERVAL_MINUTES = 5L;

    private final RestTemplate restTemplate;
    private final RestTemplate pollingRestTemplate;
    private final Jaxb2Marshaller marshaller;

    static class PollingErrorHandler extends DefaultResponseErrorHandler {
        @Override
        protected boolean hasError(HttpStatus statusCode) {
            if(statusCode == NOT_FOUND) {
                return false;
            }
            return super.hasError(statusCode);
        }
    }

    @Inject
    InternationalTradeClient(RestTemplateBuilder builder, Jaxb2Marshaller marshaller) {
        this.restTemplate = builder.rootUri("http://ec.europa.eu/eurostat/SDMX/diss-web/rest/data")
                .setReadTimeout((int) TimeUnit.MINUTES.toMillis(3))
                .setConnectTimeout((int) TimeUnit.MINUTES.toMillis(1))
                .build();
        this.pollingRestTemplate = builder.errorHandler(new PollingErrorHandler()).build();
        this.marshaller = marshaller;
    }

    public EurostatMessage request(InternationalTradeQueryFilter filter) {
        try {
            URI uri = UriComponentsBuilder.fromHttpUrl("http://ec.europa.eu/eurostat/SDMX/diss-web/rest/data")
                    .path("/" + INTERNATIONAL_TRADE_DB_ID)
                    .path("/" + buildFilterPath(filter))
                    .queryParam("startPeriod", 2016)
                    .queryParam("endPeriod", 2016)
                    .build()
                    .toUri();

            log.info("URI {}", uri);

            String rawResponse = restTemplate.getForEntity(uri, String.class).getBody();
            log.debug("raw response {}", rawResponse);

            return unmarshal(rawResponse, filter.getProducts());
        } catch(Exception e) {
            return errorMessage(e, filter.getProducts());
        }
    }

    private EurostatMessage errorMessage(Throwable cause, List<String> products) {
        EurostatMessage msg = new EurostatMessage();
        MessageCode code = new MessageCode();
        code.setCode(HttpStatus.BAD_REQUEST.value());
        log.error("Error while reading products " + products, cause);
        code.setMessageText(newArrayList("Exception " + cause, cause.toString(), products.toString()));
        MessageFooter footer = new MessageFooter();
        footer.setMessageCode(code);
        msg.setFooter(footer);
        return msg;
    }

    protected EurostatMessage unmarshal(String rawResponse) {
        EurostatMessage msg = (EurostatMessage) marshaller.unmarshal(new StreamSource(new StringReader(rawResponse)));
        log.debug("Raw msg {}", msg);
        return msg;
    }

    protected EurostatMessage unmarshal(String rawResponse, List<String> products) {
        EurostatMessage msg = unmarshal(rawResponse);
        Optional.ofNullable(msg.getFooter())
                .map(f -> f.getMessageCode().getCode())
                .ifPresent(s -> {
                    if(s != HttpStatus.OK.value()) {
                        log.warn("Error received when requesting products {}", products);
                    }
                });
        return msg;
    }

    protected EurostatMessage fromZip(ZipInputStream str) {

        try {
            str.getNextEntry();

            StringBuilder sb = new StringBuilder();
            new BufferedReader(new InputStreamReader(str))
                    .lines()
                    .forEach(sb::append);

            String xml = sb.toString();
            log.trace("SB: {}", xml);

            str.closeEntry();

            return unmarshal(xml);
        } catch (IOException e) {
            throw new EurostatServiceException("Unable to extract response from zip", e);
        }
    }

    private static String buildFilterPath(InternationalTradeQueryFilter filter) {
        return String.join(".",
                nullSafeObjectToParam(filter.getPeriod().getCode()),
                nullSafeObjectToParam(filter.getReporters()),
                nullSafeObjectToParam(filter.getPartners()),
                nullSafeObjectToParam(filter.getProducts()),
                nullSafeObjectToParam(filter.getFlows().stream()
                        .map(InternationalTradeQueryFilter.Flow::getCode)
                        .collect(Collectors.toList())),
                nullSafeObjectToParam(filter.getIndicators().stream()
                        .map(InternationalTradeQueryFilter.Indicator::name)
                        .collect(Collectors.toList())));
    }

    private static String nullSafeObjectToParam(Object obj) {
        return Optional.ofNullable(obj)
                .map(Object::toString)
                .orElse("");
    }

    private static String nullSafeObjectToParam(Collection<?> params) {
        return Optional.ofNullable(params)
                .map(p -> String.join("+", p.stream().map(Object::toString).collect(Collectors.toList())))
                .orElse("");
    }

    public EurostatMessage poll(String url) {
        byte[] bytes;
        int i = 0;

        try {
            do {
                log.info("Trying to get results from URL {} in {} minute(s)...", url, POLL_INTERVAL_MINUTES);
                Thread.sleep(TimeUnit.MINUTES.toMillis(POLL_INTERVAL_MINUTES));
                bytes = tryDownload(url);
            }
            while (bytes == null && ++i < MAX_POLL_RETRIES);
        } catch(InterruptedException e) {
            throw new EurostatServiceException(String.format("Polling of URL %s was interrupted", url), e);
        }

        return Optional.ofNullable(bytes)
                .map(b -> fromZip(new ZipInputStream(new ByteArrayInputStream(b))))
                .orElseThrow(() -> new EurostatServiceException("Downloading data from eurostat failed"));
    }

    private byte[] tryDownload(String url) {
        ResponseEntity<byte[]> response = pollingRestTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, byte[].class);
        switch(response.getStatusCode()) {
            case NOT_FOUND:
                log.debug("Results for URL {} are not yet ready", url);
                return null;
            case OK:
                log.info("Downloaded data from URL {}", url);
                return response.getBody();
            default:
                throw new EurostatServiceException(String.format("Response code %s from eurostat url %s not supported",
                        response.getStatusCode(),
                        url));
        }
    }
}
