package eu.telepski.finance.eurostat.message.control;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author memnit0
 */
@Data
@XmlRootElement
@XmlAccessorType(FIELD)
public class DataElement {

    @XmlElement(name = "ObsDimension", namespace = EurostatMessage.GENERIC_NAMESPACE)
    private DataDimension dimension;

    @XmlElement(name = "ObsValue", namespace = EurostatMessage.GENERIC_NAMESPACE)
    private DataValue value;
}
